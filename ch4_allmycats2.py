# Modified from "Automate the Boring Stuff with Python" by Al Sweigart
# program demonstrating list usage and concatenation

catNames = []
while True:
    print('Enter the name of cat ' + str(len(catNames) + 1) + ' (Or enter nothing to stop.):')
    name = input()
    if name == "":
        print('Now in reverse order!')
        negative_length = len(catNames) * -1
        i = -1
        while i >= negative_length:
            print(catNames[i])
            i -= 1
        break

    catNames = catNames +[name] # list concatenation
    print('The cat names area:')
    for name in catNames:
        print(' ' + name)





