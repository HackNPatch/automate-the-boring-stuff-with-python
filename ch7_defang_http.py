#! python3
# takes input from clipboard, looks for http(s) urls, defangs URLs, copies to clipboard, outputs to screen

import pyperclip, re

httpRegex = re.compile(r'https?://.*', re.I)
text = pyperclip.paste()

results = httpRegex.findall(text)
clipboard = ""

for result in results:
    result = result.replace('http','hxxp')
    clipboard += result + '\n'
    print(result)

pyperclip.copy(clipboard)
print(str(len(results)) + ' results returned.')
