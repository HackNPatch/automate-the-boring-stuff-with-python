# Modified from "Automate the Boring Stuff with Python" by Al Sweigart
# Fake password protected program demonstrating flow control
from random import randint

while True:
    print('Who are you?')
    name = input()
    if name.lower() != 'joe': # make the "username" not case sensitive
        continue
    print('Hello Joe. What is the password? (It is a fish.)')

    tries = 3
    realPassword = ['marlin', 'eel', 'swordfish'] # create an array of potential passwords

    while tries > 0: # allow three attempts to enter the correct password
        password = input()
        if password == realPassword[(randint(0,2))]: # randomly rotate between the three potential passwords
            print('Access granted')
            exit(0)
        print("Password incorrect")
        tries -= 1
    print("Access Denied!") # after three unsuccessful attempts, exit the program
    exit(1)

