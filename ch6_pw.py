#! python3
# pw.py - An insecure password locker program
# Modified from "Automate the Boring Stuff with Python" by Al Sweigart

PASSWORDS = {'email': 'F7minlBDDuvMJuxESSKHFhTxFtjVB6',
'blog': 'VmALvQyKAxiVH5G8v01if1MLZF3sdt',
'luggage': '12345'}

import sys, pyperclip
if len(sys.argv) < 2:
    print('Usage: python ' + sys.argv[0] + ' [account] - copy account password')
    sys.exit()

account = sys.argv[1]   # first command line arg is account name
if account in PASSWORDS:
    pyperclip.copy(PASSWORDS[account])
    print('Password for ' + account + ' copied to clipboard')
else:
    print('No account found for with name ' + account)