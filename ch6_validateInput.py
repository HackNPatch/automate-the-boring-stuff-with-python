# Modified from "Automate the Boring Stuff with Python" by Al Sweigart
# validating string input via while loops

while True:
    print('Enter your age:')
    age = input()
    if age.isdecimal():
        break
    print('Please enter a number for your age.')

while True:
    print('Select a new password (letters and numbers only):')
    password = input()
    if password.isalnum():
        break
    print('Passwords should be alphanumeric only.')