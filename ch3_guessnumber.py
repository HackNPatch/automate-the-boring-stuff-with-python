# Modified from "Automate the Boring Stuff with Python" by Al Sweigart
# This is a guess the number game.
from random import randint

secretNumber = randint(1, 20)
print('I am thinking of a number between 1 and 20.')

# Ask the player to guess 6 times.
for guessesTaken in range(1, 7):
    print('Take a guess.')

    try: # prevent the program from crashing from string-type input
        guess = int(input())
    except ValueError:
        print('Enter a number...')
        continue # this will cause guessesTaken to be incremented, but will still re-prompt for user input

    if guess < secretNumber:
        print('Your guess is too low.')
    elif guess > secretNumber:
        print('Your guess is too high.')
    else:
        break # This condition is the correct guess!

if guess == secretNumber:
    print('Good job! You guessed my number in ' + str(guessesTaken) + ' guesses!')
else:
    print('Nope. The number I was thinking of was ' + str(secretNumber))