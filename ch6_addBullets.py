# Modified from "Automate the Boring Stuff with Python" by Al Sweigart
# grabs newline separated list from user, and makes it bulleted

import sys,pyperclip

# Grabbing text from clipboard...
clip = pyperclip.paste()

# adding bullets
result = ''
lines = clip.split('\n')
for line in lines:
    result += str('- ' + line)

# outputting bulleted list to clipboard
pyperclip.copy(result)