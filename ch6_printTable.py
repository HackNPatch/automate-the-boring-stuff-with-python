# Modified from "Automate the Boring Stuff with Python" by Al Sweigart
# write a function that takes a list of strings and displays in a
# well organized, right justified table

tableData = [['apples', 'oranges', 'cherries', 'banana'],
['Alice', 'Bob', 'Carol', 'David'],
['dogs', 'cats', 'moose', 'goose']]

def printTable(table):
    # find longest item in order to set table width
    longest = 0
    for item in table:
        if len(item) > longest:
            longest = len(item)

    for item in table:
        print(str(item).center(0))

printTable(tableData)