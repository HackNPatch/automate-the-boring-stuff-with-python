# Modified from "Automate the Boring Stuff with Python" by Al Sweigart
# Magic Eight Ball

from random import randint
'''
Commenting out this section because it's inefficient and I can do it better
def getAnswer(answerNumber):
    if answerNumber == 1:
        return 'It is certain.'
    if answerNumber == 2:
        return 'It is decidedly so.'
    if answerNumber == 3:
        return 'Yes.'
    if answerNumber == 4:
        return 'Reply hazy, try again'
    if answerNumber == 5:
        return 'Ask again later.'
    if answerNumber == 6:
        return 'Concentrate and ask again.'
    if answerNumber == 7:
        return 'My reply is no.'
    if answerNumber == 8:
        return 'Outlook is not good. (Seriously, it crashes everyday.)'
    if answerNumber == 9:
        return 'Very doubtful'
'''
# define possible Eight Ball answers in the array
answers = ['It is certain.','It is decidedly so.','Yes.','Reply hazy, try again','Ask again later.','Concentrate and ask again.','My reply is no.','Outlook is not good. (Seriously, it crashes everyday.)','Very doubtful']

while True: # loop until user provides "Q" to quit
    # get question
    print('Enter a question for the Magic Eight Ball (or enter "Q" to quit): ')
    if input().lower() != 'q': # input not stored, since it's not needed. only here to make the user think something useful is happening
        # return answer
        print(answers[(randint(0,8))])
    else:
        break