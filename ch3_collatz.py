# Modified from "Automate the Boring Stuff with Python" by Al Sweigart
# A collatz sequence accepts any positive integer, does some stuff, and returns a 1

# this is the function where most of the work is done
def collatz(number):
    if number <= 0: # exit if not positive integer
        print("This won't work with " + str(number) +".")
        exit(0)
    elif number % 2 == 0: # if number is even
        return int(number / 2)
    else: # if number is odd
        return 3 * number + 1

print("Enter a number: ")

try: # catch errors related to string input
    user_num = int(input())
except: # exit with  failure code if string input
    print("Try entering a number next time...")
    exit(1)

result = user_num

while result != 1: # while sequence not finished
    result = collatz(result)
    print(int(result))