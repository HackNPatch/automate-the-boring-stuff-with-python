# Modified from "Automate the Boring Stuff with Python" by Al Sweigart
# This program says hello and asks for my name

print("Hello World!")
print("What is your name?")

# ask for name
myName = input()
print("It is good to meet you, " + myName + "!")
print("Your name is " + str(len(myName)) + " characters long.")

# ask for age
print("What is your age?")
myAge = input()
try:
    if int(myAge) > 99:
        print("Wow! You are " + str(myAge) + " years old and you know how to use a computer? Impressive!")
    else:
        print("You will be " + str(int(myAge) + 1) + " in a year.")
except: # handling exceptions in case some yahoo enters a non-integer
    print("Try entering an integer next time...")